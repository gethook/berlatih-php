<?php
function ch_case($char)
{
	$upper = strtoupper($char);
	if ($char == $upper) {
		return strtolower($char);
	}
	return $upper;
}

function tukar_besar_kecil($string)
{
	$new_string = "";
	for ($i = 0; $i < strlen($string); $i++) {
		$new_string .= ch_case($string[$i]);
	}
	return $new_string;
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo "<br>";
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo "<br>";
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo "<br>";
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo "<br>";
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>
