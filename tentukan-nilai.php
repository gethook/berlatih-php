<?php 
function tentukan_nilai($nilai)
{
	if ($nilai <= 100 && $nilai >= 85) {
		$hasil = "Sangat Baik";
	} elseif ($nilai < 85 && $nilai >= 70) {
		$hasil = "Baik";
	} elseif ($nilai < 70 && $nilai >= 60) {
		$hasil = "Cukup";
	} else {
		$hasil = "Kurang";
	}

	return $hasil;
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo "<br>";
echo tentukan_nilai(76); //Baik
echo "<br>";
echo tentukan_nilai(67); //Cukup
echo "<br>";
echo tentukan_nilai(43); //Kurang
 ?>